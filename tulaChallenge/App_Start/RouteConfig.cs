﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace tulaChallenge
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute("UploadRoute", "upload-file", new { controller = "Home", action = "Upload" });
            routes.MapRoute("ViewOrdersRoute", "view-orders", new { controller = "Home", action = "ViewOrders" });
            routes.MapRoute("DeleteOrdersRoute", "delete-orders", new { controller = "Home", action = "DeleteOrders" });


            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
