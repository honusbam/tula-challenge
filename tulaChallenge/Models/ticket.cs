﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace tulaChallenge.Models
{
    public class ticket
    {
        public int ticketID { get; set; }
        public int orderID { get; set; }
        public string ticketNumber { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public DateTime eventDate { get; set; }

        public static List<ticket> getTickets(int orderID)
        {
            List<ticket> ticketList = new List<ticket>();

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["appDB"].ConnectionString))
            {
                conn.Open();
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandTimeout = 0;
                    cmd.CommandText = "tickets_select";
                    cmd.Parameters.AddWithValue("@orderID", orderID);

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            ticket t = new ticket();
                            t.orderID = int.Parse(dr["orderID"].ToString());
                            t.ticketID = int.Parse(dr["ticketID"].ToString());
                            t.eventDate = DateTime.Parse(dr["eventDate"].ToString());
                            t.ticketNumber = dr["ticketNumber"].ToString();
                            t.firstName = dr["firstName"].ToString();
                            t.lastName = dr["lastName"].ToString();

                            ticketList.Add(t);
                        }
                    }
                }
            }

            return ticketList;
        }

        public void saveTicket()
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["appDB"].ConnectionString))
            {
                conn.Open();
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandTimeout = 0;
                    cmd.CommandText = "ticket_save";
                    cmd.Parameters.AddWithValue("@orderID", orderID);
                    cmd.Parameters.AddWithValue("@ticketNumber", ticketNumber);
                    cmd.Parameters.AddWithValue("@firstName", firstName);
                    cmd.Parameters.AddWithValue("@lastName", lastName);
                    cmd.Parameters.AddWithValue("@eventDate", eventDate);
                    cmd.ExecuteNonQuery();
                }
            }
        }
    }
}