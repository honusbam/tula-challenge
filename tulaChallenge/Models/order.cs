﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace tulaChallenge.Models
{
    public class order
    {
        public int orderID { get; set; }
        public DateTime orderUploadDate { get; set; }
        public List<ticket> tickets { get; set; }

        public static List<order> GetAllOrders() {
            List<order> orderList = new List<order>();

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["appDB"].ConnectionString))
            {
                conn.Open();
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandTimeout = 0;
                    cmd.CommandText = "orders_select";

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            order o = new order();
                            o.orderID = int.Parse(dr["orderID"].ToString());
                            o.orderUploadDate = DateTime.Parse(dr["orderUploadDate"].ToString());

                            o.tickets = ticket.getTickets(o.orderID);

                            orderList.Add(o);
                        }
                    }
                }
            }

            return orderList;
        }

        public static void DeleteAllOrders()
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["appDB"].ConnectionString))
            {
                conn.Open();
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandTimeout = 0;
                    cmd.CommandText = "orders_delete";
                    cmd.ExecuteNonQuery();                    
                }
            }
        }

        public void saveOrder()
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["appDB"].ConnectionString))
            {
                conn.Open();
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.CommandTimeout = 0;
                    cmd.CommandText = "order_save";
                    cmd.Parameters.AddWithValue("@orderID", orderID);
                    cmd.ExecuteNonQuery();
                }
            }
        }
    }
}