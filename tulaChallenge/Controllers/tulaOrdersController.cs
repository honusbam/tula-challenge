﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using tulaChallenge.Models;

namespace tulaChallenge.Controllers
{

    [RoutePrefix("api/Orders")]
    public class tulaOrdersController : ApiController
    {
        [Route("GetOrders")]
        public List<order> GetOrders()
        {
            return order.GetAllOrders();
        }

        [Route("DeleteOrders")]
        public void DeleteOrders()
        {
            order.DeleteAllOrders();
        }

        [Route("ProcessOrderFile")]
        public HttpStatusCode ProcessOrderFile(HttpRequestMessage fileData)
        {
            //this could be better to save file to a UNC path and then use bulk insert, but for now, just reading a line at a time

            if (fileData != null)
            {
                using (var sr = new StreamReader(HttpContext.Current.Request.InputStream))
                {
                    sr.BaseStream.Position = 0;

                    int i = 0;
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        i++;

                        if (i > 4)
                        {    //hokey way to skip headers, but not ideal
                            var details = line.Split(',');
                            if (details.Length > 1)
                            {
                                ticket t = new ticket()
                                {
                                    orderID = int.Parse(details[0])
                                    ,
                                    firstName = details[1]
                                    ,
                                    lastName = details[2]
                                    ,
                                    ticketNumber = details[3]
                                    ,
                                    eventDate = DateTime.Parse(details[4])
                                };

                                t.saveTicket();
                            }
                        }
                    }
                }

                return HttpStatusCode.Accepted;
            }
            else {
                return HttpStatusCode.InternalServerError;
            }
        }
    }
}