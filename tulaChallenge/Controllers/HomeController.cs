﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using tulaChallenge.Models;

namespace tulaChallenge.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Upload()
        {
            ViewBag.Message = "Upload Page.";

            return View();
        }

        public ActionResult DeleteOrders()
        {
            ViewBag.Message = "Delete Orders";

            var o = new Controllers.tulaOrdersController().GetOrders();
            int orderCount = 0;

            if (o != null && o.Count > 0)
            {
                orderCount = o.Count;
            }

            return View(orderCount);
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]    //come back to this and wire it up for form data
        public JsonResult uploadOrdersFile()
        {
            //post from form could go directly to api controller
            //chose this route to illustrate external api post
            bool didPost = false;

            if (Request.Files.Count > 0)
            {
                HttpPostedFileBase file = Request.Files[0];

                using (var client = new HttpClient())
                {
                    using (var content = new MultipartFormDataContent())
                    {
                        byte[] Bytes = new byte[file.InputStream.Length + 1];
                        file.InputStream.Read(Bytes, 0, Bytes.Length);
                        var fileContent = new ByteArrayContent(Bytes);
                        fileContent.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment") { FileName = file.FileName };
                        content.Add(fileContent);
                        var requestUri = "http://localhost:61720/api/Orders/ProcessOrderFile";
                        var result = client.PostAsync(requestUri, content).Result;

                        if (result.StatusCode == System.Net.HttpStatusCode.Accepted)
                        {
                            didPost = true;
                        }
                    }

                }
            }

            return Json(didPost, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public void deleteAllOrders()
        {
            tulaOrdersController order = new Controllers.tulaOrdersController();
            order.DeleteOrders();
        }

        public ActionResult ViewOrders()
        {
            ViewBag.Message = "View orders page.";

            tulaOrdersController order = new Controllers.tulaOrdersController();

            var o = order.GetOrders();

            if (o != null && o.Count > 0) {
                o = o.OrderBy(x => x.orderID).ToList();
            }

            return View(o);
        }
    }
}