﻿// Setup CSRF safety for AJAX:
$.ajaxPrefilter(function (options, originalOptions, jqXHR) {
    if (options.type.toUpperCase() === "POST") {
        // We need to add the verificationToken to all POSTs
        var token = $("input[name^=__RequestVerificationToken]").first();
        if (!token.length) return;

        var tokenName = token.attr("name");

        // If the data is JSON, then we need to put the token in the QueryString:
        if (!options.contentType || options.contentType.indexOf('application/json') === 0) {
            // Add the token to the URL, because we can't add it to the JSON data:
            options.url += ((options.url.indexOf("?") === -1) ? "?" : "&") + token.serialize();
        }
        else if (!options.data) {
            options.data = token.serialize();
        }
        else if (typeof options.data === 'string' && options.data.indexOf(tokenName) === -1) {
            // Append to the data string:
            options.data += (options.data ? "&" : "") + token.serialize();
        }
    }
});

$(function () {
    $("#delConf").hide();
    $("#uploadCompleteDiv").hide();
    $("div[id^=tickets_").hide();

    $("#delAllBtn").click(function (e) {
        e.preventDefault();
        $.ajax({
            type: "POST"
            , url: "/home/deleteAllOrders"
            , complete: function () {
                $("#preDel").hide();
                $("#delConf").show();
            }
        });
    });

    $(".orderDiv").click(function (e) {
        e.preventDefault();
        var orderid = $(this).data("orderid");

        if ($("#tickets_" + orderid).is(":hidden")) {
            $("#tickets_" + orderid).show(750);
        } else {
            $("#tickets_" + orderid).hide(250);
        }
    });

    $("#formUploadBtn").click(function (e) {
        e.preventDefault();

        var fileName = $("#customFile").val();
        if (fileName && fileName.length > 0) {
            var formData = new FormData();
            formData.append('file', $('#customFile')[0].files[0]);

            $.ajax({
                url: '/home/uploadOrdersFile'
                , type: 'POST'
                , data: formData
                , processData: false
                , contentType: false
                , complete: function (data) {
                    $("#uploadDiv").hide();
                    $("#uploadCompleteDiv").show();
                }
            });
        } else {
            alert("Please select a file for upload before continuing!");
        }
    });
});